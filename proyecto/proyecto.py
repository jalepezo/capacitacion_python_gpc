# PROYECTO: OBTENER INFORMACION FILTRADA DEL MEF
# PASO 1: DESCARGAR DATOS MEDIANTE LA API PARA LOS AÑOS 2018-2021 PARA CUSCO
import urllib.request

# Descarga con filtro CUSCO para 2018-2021

url_2018 = 'https://datosabiertos.mef.gob.pe/api/3/action/datastore_search?resource_id=d73016f9-09d2-46e3-8eab-9ef4b931f9a3&limit=10&include_total=false&filters={"DEPARTAMENTO_EJECUTORA_NOMBRE":"CUSCO"}'


with urllib.request.urlopen(url_2018) as response:
    print(response.read())

    
# crear un df para 2018

mef_2018 = res_2018.json()
records_2018 = mef_2018.get('result').get('records')
df_2018 = pd.DataFrame(records_2018)


# crear un df para 2019

mef_2019 = res_2019.json()
records_2019 = mef_2019.get('result').get('records')
df_2019 = pd.DataFrame(records_2019)

# verificando mi descarga
df_2019.head()
#verificar nro de filas
df_2019.columns
df_2019["DEPARTAMENTO_EJECUTORA_NOMBRE"]

# PASO 2: GUARDAR COMO UN CSV
df_2019.to_csv('semana_4/ejemplo_2019.csv', na_rep='NA')
# PARA WINDOWS: semana_4
Path.cwd()

df_2019.to_csv('semana_4/ejemplo_2018.csv', na_rep ='NA')
# Leer  el CSV
df_2018 = pd.read_csv('semana_4/ejemplo_2018.csv', na_values = 'NA')

# PASO 3: DATA WRANGLING
# MERGE

# GROUP BY

# CONVERTIR MENSUAL A ANUAL

# EST DESCRIPTIVA

# PASO 4: EXPORTAR A EXCEL
