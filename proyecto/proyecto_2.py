# PROYECTO: OBTENER INFORMACION FILTRADA DEL MEF
# PASO 1: DESCARGAR DATOS MEDIANTE LA API PARA LOS AÑOS 2018-2021 PARA CUSCO
import requests
import pandas as pd
from pathlib import Path
import os
# Descarga con filtro CUSCO para 2018-2021
api = 'https://datosabiertos.mef.gob.pe/api/3/action/datastore_search'

param_2018 = {
    'resource_id':'d73016f9-09d2-46e3-8eab-9ef4b931f9a3',
    'filters' : '{"DEPARTAMENTO_EJECUTORA_NOMBRE" : "CUSCO"}',
    'include_total' : False,
    'limit':'50'
   }

param_2019 = {
    'resource_id':'792c32c7-54a7-4d7b-a944-767520159184',
    'filters' : '{"DEPARTAMENTO_EJECUTORA_NOMBRE" : "CUSCO"}',
    'include_total' : False,
    'limit':'50'
   }

param_2020 = {
    'resource_id':'5a24370e-da9f-4519-87e7-a9565c08670f',
    'filters' : '{"DEPARTAMENTO_EJECUTORA_NOMBRE" : "CUSCO"}',
    'include_total' : False,
    'limit':'300000'
   }

param_2021_1= {
    'resource_id':'b88f6ee5-762a-41f1-814a-38beb289d404',
    'filters' : '{"DEPARTAMENTO_EJECUTORA_NOMBRE" : "CUSCO"}',
    'include_total' : False,
    'limit':'300000',
   }
param_2021_2= {
    'resource_id':'b88f6ee5-762a-41f1-814a-38beb289d404',
    'filters' : '{"DEPARTAMENTO_EJECUTORA_NOMBRE" : "CUSCO"}',
    'include_total' : False,
    'offset':'300000',
    'limit':'75000'
    }

# descarga para 2018-2021
res_2018 = requests.get(api, params=param_2018)
res_2019 = requests.get(api, params=param_2019)
res_2020 = requests.get(api, params=param_2020)
res_2021 = requests.get(api, params=param_2021)

# verificando la conexion 
res_2018.raise_for_status()
res_2019.raise_for_status()
res_2020.raise_for_status()
res_2021.raise_for_status()

# crear un df para 2018

mef_2018 = res_2018.json()
records_2018 = mef_2018.get('result').get('records')
df_2018 = pd.DataFrame(records_2018)


# crear un df para 2019

mef_2019 = res_2019.json()
records_2019 = mef_2019.get('result').get('records')
df_2019 = pd.DataFrame(records_2019)

# verificando mi descarga
df_2018.head()
df_2018.columns
df_2018.index
df_2018["DEPARTAMENTO_EJECUTORA_NOMBRE"]

df_2019.head()
df_2019.columns
df_2019.index
df_2019["DEPARTAMENTO_EJECUTORA_NOMBRE"]

# PASO 2: GUARDAR COMO UN CSV
df_2018.to_csv('proyecto/ejemplo_2018.csv', na_rep='NA')
df_2019.to_csv('proyecto/ejemplo_2019.csv', na_rep='NA')
df_total.to_csv('proyecto/ejemplo_total.csv')

# Leer  el CSV
df_2018 = pd.read_csv('proyecto/ejemplo_2018.csv', na_values = 'NA')
df_2019 = pd.read_csv('proyecto/ejemplo_2019.csv', na_values = 'NA')
df_total = pd.read_csv('proyecto/ejemplo_total.csv', na_values='NA')

# PASO 3: DATA WRANGLING
# MERGE: unimos nuestros data sets en el data set total
df_total = pd.merge(df_2018, df_2019, how='outer')
df_total_2 = pd.merge(df_total,df_2020, how='outer')
# compruebo que df_total tiene toda la informacion necesaria
print(df_total['ANO_EJE'].unique())
print(df_total['MES_EJE'].unique())
df_total.head(10)
df_total.columns
df_total.index

# DROPEANDO VARIAS COLUMAS
# 1. creo una lista que contenga las columas que quiero dropear,en este caso as sgtes tres columnas: 

dropear = ['MES_EJE','TIPO_GOBIERNO', 'TIPO_GOBIERNO_NOMBRE']

# 2.aplico drop y guardo lo deseado en el df_clean
df_clean = df_total.drop(dropear, axis=1)

# SELECCIONANDO VARIAS COLUMNAS
# 1. creo una lista que contenga las columas que quiero seleccionar

seleccion = ['DISTRITO_EJECUTORA_NOMBRE','TIPO_RECURSO','RUBRO_NOMBRE','MONTO_CERTIFICADO'] 
# 2. Corto mi data frame

df_selecto = df_total[seleccion]

# GROUP BY: Seleccionando 'ANO_EJE', calculo la suma y muestro todas las columnas

df_agrupado =df_total.groupby('ANO_EJE').sum().reset_index()


df_agrupado_2 = df_total.groupby('PRODUCTO_PROYECTO').sum().reset_index()

# SEGUNDA EJEMPLO:  GROUP BY DOBLE 'ANO_EJE' y la media de 'MONTO_PIM'

df_total.groupby('ANO_EJE').MONTO_PIM.mean()


# EST DESCRIPTIVA
df_agrupado_media = df_total.groupby('ANO_EJE').mean()

# PASO 4: EXPORTAR A EXCEL
df_agrupado_media.to_excel('proyecto/resumen_cusco.xlsx')
