# Vamos a ver un poco de numpy - que es una libreria que construye a
# pandas y su modo de manejar y leer la informacion. 
# Numpy trabaja con  objetos llamados ''ndarray'' con los que podemos realizar operaciones
# matemáticas mucho más rapido que en Python ordinario
# Creemos nuestro primer array
import numpy as np

lista = [6, 18, 18.2, 30]

array_ejemplo = np.array(lista)
array_ejemplo
type(array_ejemplo)
# con numpy podemos trabajar con matrices (Matlab)
matriz = [[1, 2, 3, 4], [5, 6, 7, 8]]
matriz_ejemplo = np.array(matriz)
matriz_ejemplo
matriz_ejemplo.shape

# Aritmetica de ndarrays
# Python no puede trabajar directamente con matrices y entre matrices y escalares, Numpy permite adoptar esta funcionalidad de forma intuitiva
matriz_2 = np.array([[1., 2., 3.], [4., 5., 6.]])
matriz_3 = np.array([[-1.,-2,-3] , [2.,4.,7.]])
matriz_2 + matriz_3

#la multiplicacion elemento por elemento
matriz_2 * matriz_3

#multiplicacion por escalares
matriz_2 * 0.5

#seleccionando data partes por partes
ejemplo = np.arange(10)
ejemplo[8]
ejemplo[0:6]
ejemplo[0:4] = 0
ejemplo_hunk = ejemplo[0:4]
ejemplo_hunk[0]=2022
# Numpy no copia estos cambios a memoria, sino que los tiene como "vistas"
