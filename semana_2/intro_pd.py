# Pandas es la libreria que contiene formatos de data y metodos de manipulacion
# que ayudan a limpiar y analizar data en Python

import pandas as pd
import numpy as np
# trabajaremos con dos objetos de pandas: las series y los data frames

# SERIES
# Las series son arrays uno-dimensionales que contienen una serie de valores
# asociados con una serie de indices (como los diccionarios)

ejemplo = pd.Series([1, 2, "b"])
ejemplo[1]

# como los diccionarios, podemos extraer o llamar los valores
ejemplo.values
ejemplo.index
# podemos crear nuestros propios indices
ejemplo_2 = pd.Series(['naranja', 'papaya', 'manzana'], index=['n', 'p', 'm'])
ejemplo_2
ejemplo_2.index
# Como en diccionarios, podemos usar las keys o indices para buscar valores dentro de la Serie:
ejemplo_2['n']
ejemplo_2[['p', 'm']]

# Operaciones con los valores de una Serie

ejemplo_3 = pd.Series([4, 7, 8, -1], index=['a', 'b', 'c', 'd'])

ejemplo_3['b']
ejemplo_3[ejemplo_3 < 0]
ejemplo_3 + 1

# podemos convertir los diccionarios a Series de Pandas

ejemplo_dic = {'lima':20, 'cusco':10, 'iquitos':30, 'tacna':25}
ejemplo_series = pd.Series(ejemplo_dic)

# podemos sumar dos series
ejemplo_dic2 = {'trujillo': 15, 'cusco': 20, 'lima': 25, 'tacna': 20}

ejemplo_series2 = pd.Series(ejemplo_dic2)
promedio_temp = 0.5*(ejemplo_series + ejemplo_series2)

# observan que se han creado Nan (valores NA) para el resultado, para los elementos que no estan en la intereseccion de las series (iquitos y trujillo)
pd.isnull(promedio_temp)


# nombres para la columna y filas de nuestra Serie: 
promedio_temp.name = 'temperatura_promedio'
promedio_temp.index.name = 'ciudad'

# dataframes
# un dataframe es un representacion tabular y rectangular del data, compuesta por filas y columnas
data = {'ciudad':['Tacna','Tacna','Ica','Ica','Cusco','Cusco'],
        'year':[2016, 2017, 2016, 2017, 2016, 2017],
        'gdp': [20, 15, 30, 30, 20, 25]}
frame = pd.DataFrame(data)

# podemos ver las cinco primeras filas con head()
frame.head()

#ordenando columnas
pd.DataFrame(data, columns=['year', 'ciudad','gdp'])

#ponemos indices especificos
pd.DataFrame(data, index=['a','b','c','d','e','f'])

#podemos ver las columnas
frame.columns
#extraemos las columnas
frame['gdp']
frame.gdp

#extraemos filas
frame.loc[[1]]

#creando nuevas columnas con un valor constante
frame['pop'] = 20.8

# Adicionando una lista o Serie como una columna a un dataframe
#La longitud del objeto  debe ser igual al numero de filas del data frame.
# Cuando juntamos una Serie como columna, los indices de la serie se alinean con los indices del dataframe, los que sobren se les deja como NA

nueva_col = pd.Series(['a','b','c','d','e','f'], index=[0,1,2,3,4,5])
frame['crecimiento'] = nueva_col

#borrar columnas

del(frame['pop'])

#leyendo diccionarios acunados, los indices mayores son las columnas del df
crecimiento = {'Tacna': {2016: 2.5, 2017: 2.0}, 'Ica': {2016: 2.5, 2017: 1.8, 2018: 1.5}}

frame2 = pd.DataFrame(crecimiento)

#podemos seleccionar los indices que queremos usar

pd.DataFrame(crecimiento, index=[2016,2017])

# transponiendo un data frame
frame2.T

#nombres a las columnas y filas

frame2.index.name = 'year'
frame2.columns.name = 'ciudad'

#extrayendo valores

frame2.values

#indices
#Pandas trata a los indices como objetos especiales
obj = pd.Series(range(3), index=['a','b','c'])
index = obj.index
# los indices son inmutables una vez creados en Series o DataFrames
index[0] = 'x'

#Verificando la existencia de una columna o variable
frame2.columns
'Cusco' in frame2.columns

#verificando la existencia de una observacion en una fila 
frame2.index

2020 in frame2.index

