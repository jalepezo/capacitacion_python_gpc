# En este video explicare como filtrar e identificar valores na en pandas
# Primero cargo mis paquetes
import requests
import json
import pandas as pd
from numpy import nan as NA
#usare mi ejemplo del MEF para el 2016
url_2016 = "https://datosabiertos.mef.gob.pe/api/3/action/datastore_search?resource_id=2a9ffe69-cdd2-4c48-a6b6-859bd02d02aa&limit=10"
res_2016 = requests.get(url_2016)
print(res_2016.status_code)
mef_2016 = res_2016.json()
mef_2016.keys()
mef_2016.get('result').keys()
records_2016 = mef_2016.get('result').get('records')

df_2016 = pd.DataFrame(records_2016)
df_2016.head()

#usando .dropna() elimina toda fila que contiene POR LO MENOS un NA

clean = df_2016.dropna()

data = pd.DataFrame([[1.,2, 3.],[1.,NA,NA],[NA,NA,NA],[NA,5.,7]])

data.dropna()

#usando la opcion how='all' dropea las filas cuyos valores SON TODOS NA

df_2016.dropna(how='all')
data.dropna(how='all')

#para dropear valores NA en las columnas usamos la opcion axis=1

df_2016.dropna(axis=1, how="all")

data2 = data.T

data2.dropna(axis=1, how='all')
