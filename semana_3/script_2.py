#en este video veremos como leer y guardar info en python
#primero replico lo que hice para descargar la data en python desde del MEF
import requests
import pandas as pd
from pathlib import Path

#usare mi ejemplo del MEF para el 2016
url_2016 = "https://datosabiertos.mef.gob.pe/api/3/action/datastore_search?resource_id=2a9ffe69-cdd2-4c48-a6b6-859bd02d02aa&limit=100"
res_2016 = requests.get(url_2016)
print(res_2016.status_code)
mef_2016 = res_2016.json()
records_2016 = mef_2016.get('result').get('records')

df_2016 = pd.DataFrame(records_2016)

df_2016.head()

# csv es el formato de data más común, las columas se separan por comas (,)
# De forma general podemos leer tables (separados por tabs, | ...)
# adicionalmente podemos usar la funcion read_excel, read_stata, read_sas, todas estas funciones son parte de Pandas
#Leer un csv desde un archivo

df = pd.read_csv('~/Documents/job/propuesta_ciudadana/python_asesoria/semana_3/ex1.csv')

 
#Indicar 'que' consideramos como valor nulo

df = pd.read_csv('semana_3/ex1.csv', na_values=['hello'])
df

# exportando a csv

df_2016.to_csv('semana_3/data_2016.csv', na_rep='NA')


# podemos guardar solo una porcion del dataset

seleccion = ['DISTRITO_EJECUTORA','MONTO_PIM']

df_short = df_2016[seleccion]

df_2016.to_csv('semana_3/data_2016_short.csv', columns=seleccion)

#trabajando con archivos grandes
#indicamos que solo ver unas filas de un archivo por defecto:

pd.options.display.max_rows = 10

#tambien solo podemos leer un numero selecto de filas
pd.read_csv('~/Documents/job/propuesta_ciudadana/python_asesoria/semana_3/data_2016.csv', nrows=30)

# para trabajar con excel tienes que installar los paquetes : xlrd y openpyxl
# usando un excel en python:

excel = pd.ExcelFile('semana_3/ex1.xlsx')
#una vez creado este objeto excel puedes leerlo como un df
pd.read_excel(excel,'Sheet1')

#directamente
frame = pd.read_excel('semana_3/ex1.xlsx','Sheet1')

# guardando df de pandas a un excel
writer = pd.ExcelWriter('semana_3/ex2.xlsx')
frame.to_excel(writer,'Sheet1')
writer.save()

#directamente
df_short.to_excel('semana_3/df_short.xlsx')
