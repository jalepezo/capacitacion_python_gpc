# En este script usaremos el API del MEF para descargar la data
# Yo trabajare con el año 2016 como ejemplo
resource: 2a9ffe69-cdd2-4c48-a6b6-859bd02d02aa

# PRIMERO: IMPORTAMOS REQUEST PACKAQE
# requests nos ayuda a tener informacion de cualquier pagina web

import requests
import json

# EJEMPLO CON FOX
# Una API nos da la información resumida (json) - no es lo mismo que webscrapping

response = requests.get("https://randomfox.ca/floof")

# Tenemos que obtener 200 para ver si hemos tenido exito:

print(response.status_code)

# con el comando de abajo obtenemos un diccionario en python
fox = response.json()
print(fox)

# podemos usar claves para buscar en el diccionario, por ejemplo,
print(fox['image'])

# usando el API MEF 2016 con los 10 primeros registros, observa la notacion de este link:
url_2016 = "https://datosabiertos.mef.gob.pe/api/3/action/datastore_search?resource_id=2a9ffe69-cdd2-4c48-a6b6-859bd02d02aa&limit=10"

res_2016 = requests.get(url_2016)
print(res_2016.status_code)

# Convertir request objecto a diccionario python:
mef_2016 = res_2016.json()
type(mef_2016)
print(mef_2016)

list(mef_2016.keys())
records_2016 = mef_2016.get('result').get('records')
type(records_2016)
