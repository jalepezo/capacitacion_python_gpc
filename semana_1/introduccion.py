# Los diccionarios nos ayudan a trabajar con la información de los API.
# Los diccionarios son objetos con extension dict
# Los diccionarios se conforman por "indices" y por "valores"
peces = {"g": "goldfish", "p": "paiche", "t": "trucha"}
print(peces)
type(peces)

# Operaciones con diccionarios
lista_precios = {"manzana": 1, "mango": 3, "uva": 4}
# ACCEDEMOS A UN VALOR USANDO UN INDICE - KEY

lista_precios["manzana"]
lista_precios["uva"]

# CAMBIAMOS UN VALOR DICCIONARIO
lista_precios["uva"] = 5

# AÑADIMOS ELEMENTOS
lista_precios["naranja"] = 2

# ELIMINAMOS ELEMENTOS

lista_precios["zanahoria"] = 6
lista_precios.pop("zanahoria")
print(lista_precios)

# Ejemplo de la funcion dict() para crear diccionarios
lista_compras = {"papa": 1, "tomate": 0.5, "arroz": 3}
lista_compras = dict(papa=1, tomate=0.5, arroz=3)
type(lista_compras)

# Listado de keys y valores uno por uno
compras_semana = list(lista_compras.keys())
consumo_semana = list(lista_compras.values())

# Union de diccionarios
lista_compras.update(lista_precios)
print(lista_compras)
